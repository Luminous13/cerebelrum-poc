import Link from 'next/link';

import styles from './page.module.css';
import { Button } from '../components/atoms';

export default function Index() {
  return (
    <div className="container mx-auto flex flex-col justify-center items-center h-screen">
      <div className="text-2xl mb-20">Render Products Page</div>
      <div className="w-full flex justify-center items-center gap-4">
        <Link href="/products">
          <Button type="secondary">Server-Side</Button>
        </Link>
        <Link href="/products/client-side">
          <Button type="primary">Client-Side</Button>
        </Link>
      </div>
    </div>
  );
}
