type Props = {
  value: string | number;
};

const CounterText: React.FC<Props> = ({ value }) => {
  const counterValue = {
    '--value': value,
  } as React.CSSProperties;

  return (
    <span className="countdown font-mono text-6xl">
      <span style={counterValue}></span>
    </span>
  );
};

export default CounterText;
