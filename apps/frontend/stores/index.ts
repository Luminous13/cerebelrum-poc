import { create } from 'zustand';

import { CounterSlice, createCounterSlice } from './counter.slice';

export const useGlobalStore = create<CounterSlice>((...a) => ({
  ...createCounterSlice(...a),
}));
