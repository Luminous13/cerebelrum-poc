import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SnakeNamingStrategy } from 'typeorm-naming-strategies';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { Product } from './products/entities/product.entity';
import { ProductsModule } from './products/products.module';

const defaultDBOptions = {
  host: process.env.DB_HOST,
  port: +process.env.DB_PORT,
  username: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  namingStrategy: new SnakeNamingStrategy(),
  autoLoadEntities: true,
  synchronize: false,
};

@Module({
  imports: [
    TypeOrmModule.forRoot({
      ...defaultDBOptions,
      type: 'postgres',
      database: process.env.DB_DATABASE,
      entities: [Product],
    }),
    ProductsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
