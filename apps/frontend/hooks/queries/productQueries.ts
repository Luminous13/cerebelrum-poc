import { ProductDto } from '@cerebelrum/api-lib';
import { useQuery } from '@tanstack/react-query';
import { QUERY_KEYS } from 'apps/frontend/constants/queryKeys';
import { getAllProducts } from 'apps/frontend/services/productServices';

export const useGetAllProducts = () =>
  useQuery<ProductDto[]>({
    queryKey: [QUERY_KEYS.GET_ALL_PRODUCTS],
    queryFn: () => getAllProducts(),
  });
