import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsNumber } from 'class-validator';

export class RatingDto {
  @ApiProperty()
  @IsNumber()
  rate: number;

  @ApiProperty()
  @IsInt()
  count: number;
}
