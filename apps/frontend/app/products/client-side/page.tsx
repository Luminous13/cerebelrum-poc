'use client';
import ClientProductsPage from './ClientProductsPage';

const Index: React.FC = () => {
  return (
    <div className="w-screen box-border text-center">
      <h1 className="text-4xl font-bold mb-20">Client Side</h1>
      <ClientProductsPage />
    </div>
  );
};

export default Index;
