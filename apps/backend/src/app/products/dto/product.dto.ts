import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsInt, IsNumber, IsObject } from 'class-validator';

import { RatingDto } from './rating.dto';

export class ProductDto {
  @ApiProperty()
  @IsInt()
  id: number;

  @ApiProperty()
  @IsString()
  title: string;

  @ApiProperty()
  @IsNumber()
  price: number;

  @ApiProperty()
  @IsString()
  description: string;

  @ApiProperty()
  @IsString()
  category: string;

  @ApiProperty()
  @IsString()
  image: string;

  @ApiProperty()
  @IsObject()
  rating: RatingDto;
}
