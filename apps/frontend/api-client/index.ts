import axios from 'axios';
import { AxiosError, AxiosRequestConfig } from 'axios';
import Cookies from 'js-cookie';

const removeTrailingSlash = (url: string) =>
  url.endsWith('/') ? url.slice(0, -1) : url;

export const API_BASE_URL = removeTrailingSlash(
  process.env.NEXT_PUBLIC_API_BASE_URL ?? 'http://127.0.0.1:3000'
);

export function interceptErrorResponse(error: AxiosError) {
  console.error(error);
  return Promise.reject(error);
}

function interceptAuthRequest(config: AxiosRequestConfig): AxiosRequestConfig {
  const token = Cookies.get('access_token');

  config.headers = {
    ...config.headers,
    credentials: 'include',
  };

  if (token) {
    config.headers = {
      ...config.headers,
      withCredentials: true,
      Authorization: `Bearer ${token}`,
    };
  }

  return config;
}

export function createApiClient(
  baseURL: string,
  interceptResponse: unknown = interceptAuthRequest
) {
  const axiosInstance = axios.create();

  axiosInstance.interceptors.response.use((res) => res, interceptErrorResponse);

  axiosInstance.interceptors.request.use(
    interceptResponse as any,
    interceptErrorResponse
  );

  return {
    axiosInstance,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    use: <ApiType>(api: new (...args: any[]) => ApiType): ApiType =>
      new api(undefined, baseURL, axiosInstance),
  };
}

const ApiClient = createApiClient(API_BASE_URL || '', interceptAuthRequest);

export default ApiClient;
