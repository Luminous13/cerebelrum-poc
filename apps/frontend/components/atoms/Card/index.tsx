import { ProductDto } from '@cerebelrum/api-lib';
import Image from 'next/image';

type Props = {
  product: ProductDto;
};

const Card: React.FC<Props> = (props) => {
  const { product } = props;
  const { image, title, price } = product;

  return (
    <>
      <div className="card card-side bg-base-100 shadow-xl mb-5 bg-white">
        <figure>
          <Image width={150} height={150} src={image} alt={title} />
        </figure>
        <div className="card-body">
          <h2 className="card-title">{title}</h2>
          <p className="font-bold text-2xl text-red-700">${price}</p>
          <div className="card-actions justify-end">
            <button className="btn btn-primary">Buy</button>
          </div>
        </div>
      </div>
    </>
  );
};

export default Card;
