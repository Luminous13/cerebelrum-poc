import { MigrationInterface, QueryRunner } from "typeorm";

export class InitialProductsEntity1698723920876 implements MigrationInterface {
    name = 'InitialProductsEntity1698723920876'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            CREATE TABLE "product" (
                "id" SERIAL NOT NULL,
                "title" text NOT NULL,
                "price" numeric NOT NULL,
                "description" text,
                "category" text,
                "image" text,
                "rating" jsonb,
                CONSTRAINT "PK_bebc9158e480b949565b4dc7a82" PRIMARY KEY ("id")
            )
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            DROP TABLE "product"
        `);
    }

}
