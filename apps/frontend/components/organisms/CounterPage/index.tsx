'use client';
import { Button, CounterText } from '../../atoms';
import { useGlobalStore } from '../../../stores';

const CounterPage: React.FC = () => {
  const { count, increment, decrement } = useGlobalStore((state) => state);

  return (
    <div className="w-screen h-screen flex items-center justify-center">
      <div className="flex items-center justify-between w-52">
        <Button type="error" onClick={decrement}>
          -
        </Button>
        <CounterText value={count} />
        <Button type="primary" onClick={increment}>
          +
        </Button>
      </div>
    </div>
  );
};

export default CounterPage;
