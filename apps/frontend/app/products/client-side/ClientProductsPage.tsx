import { Card } from '../../../components/atoms';
import { useGetAllProducts } from '../../../hooks/queries/productQueries';

const ClientProductsPage: React.FC = () => {
  const { data: listProducts, isLoading } = useGetAllProducts();

  if (isLoading) return <div>Loading...</div>;

  return (
    <div className="w-screen h-screen flex justify-center">
      <div className="w-1/2 min-w-500">
        {listProducts &&
          listProducts.map((product) => (
            <Card key={product.id} product={product} />
          ))}
      </div>
    </div>
  );
};

export default ClientProductsPage;
