import { ReactNode, MouseEventHandler } from 'react';

type Props = {
  type: 'primary' | 'secondary' | 'error';
  children: ReactNode;
  onClick?: MouseEventHandler<HTMLButtonElement>;
};

const Button: React.FC<Props> = ({ type = 'primary', children, onClick }) => {
  const buttonTypes = {
    primary: 'btn-primary',
    secondary: 'btn-secondary',
    error: 'btn-error',
  };

  return (
    <button className={`btn ${buttonTypes[type]} text-2xl`} onClick={onClick}>
      {children}
    </button>
  );
};

export default Button;
