import { ProductDto, ProductsApi } from '@cerebelrum/api-lib';

import ApiClient from '../api-client';

const productsApi = ApiClient.use(ProductsApi);

export async function getAllProducts(): Promise<ProductDto[]> {
  try {
    const response = await productsApi.productsControllerFindAll();

    return response.data;
  } catch (error) {
    throw new Error(`Failed to fetch products: ${error}`);
  }
}
