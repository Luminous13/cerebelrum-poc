import { Card } from 'apps/frontend/components/atoms';
import { getAllProducts } from 'apps/frontend/services/productServices';

export default async function Index() {
  const listProducts = await getAllProducts();

  return (
    <div className="w-screen flex flex-col items-center justify-center box-border text-center">
      <h1 className="text-4xl font-bold mb-20">Server Side</h1>
      <div className="w-1/2 min-w-500">
        {listProducts &&
          listProducts.map((product) => (
            <Card key={product.id} product={product} />
          ))}
      </div>
    </div>
  );
}
